package main

import (
	"fmt"
)

func main()  {
	var miCafe = Cafe{ nombre:"espresso", precio: 5.22, azucar: false, leche:0}

	fmt.Print(miCafe)
}

type Cafe struct {
	nombre string
	precio float64
	azucar bool
	leche int
}